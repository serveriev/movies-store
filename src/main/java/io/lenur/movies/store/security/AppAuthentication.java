package io.lenur.movies.store.security;

import io.lenur.movies.store.domain.User;
import io.lenur.movies.store.dto.request.RegistrationRequestDto;

public interface AppAuthentication {
    String login(String email, String password);

    User registration(RegistrationRequestDto dto);
}
