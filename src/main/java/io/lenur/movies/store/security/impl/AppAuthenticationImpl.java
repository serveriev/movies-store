package io.lenur.movies.store.security.impl;

import io.lenur.movies.store.domain.Role;
import io.lenur.movies.store.domain.RoleName;
import io.lenur.movies.store.domain.User;
import io.lenur.movies.store.dto.request.RegistrationRequestDto;
import io.lenur.movies.store.security.AppAuthentication;
import io.lenur.movies.store.security.JwtTokenProvider;
import io.lenur.movies.store.service.domain.BasketService;
import io.lenur.movies.store.service.domain.RoleService;
import io.lenur.movies.store.service.domain.UserService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AppAuthenticationImpl implements AppAuthentication {
    private final UserService userService;
    private final BasketService basketService;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider tokenProvider;
    private final PasswordEncoder passwordEncoder;
    private final RoleService roleService;

    public AppAuthenticationImpl(
            UserService userService,
            BasketService basketService,
            AuthenticationManager authenticationManager,
            JwtTokenProvider tokenProvider,
            PasswordEncoder passwordEncoder, RoleService roleService) {
        this.userService = userService;
        this.basketService = basketService;
        this.authenticationManager = authenticationManager;
        this.tokenProvider = tokenProvider;
        this.passwordEncoder = passwordEncoder;
        this.roleService = roleService;
    }

    @Override
    public String login(String email, String password) {
        UsernamePasswordAuthenticationToken authToken =
                new UsernamePasswordAuthenticationToken(email, password);
        Authentication authentication = authenticationManager.authenticate(authToken);

        final UserDetails userDetails = (UserDetails)authentication.getPrincipal();
        return tokenProvider.generateToken(userDetails);
    }

    @Override
    public User registration(RegistrationRequestDto dto) {
        String passwordEncoded = passwordEncoder.encode(dto.getPassword());

        User user = new User();
        user.setEmail(dto.getEmail());
        user.setPassword(passwordEncoded);
        for (RoleName roleName : dto.getRoles()) {
            Role role = roleService.getByName(roleName);
            user.addRole(role);
        }

        userService.create(user);
        basketService.create(user);

        return user;
    }
}
