package io.lenur.movies.store.mapper;

import io.lenur.movies.store.domain.Basket;
import io.lenur.movies.store.domain.Ticket;
import io.lenur.movies.store.dto.response.BasketResponseDto;

public class BasketMapper {
    public static BasketResponseDto toResponse(Basket basket) {
        BasketResponseDto dto = new BasketResponseDto();
        dto.setId(basket.getId());
        dto.setUser(UserMapper.toResponse(basket.getUser()));

        for (Ticket ticket : basket.getTickets()) {
            dto.addTicket(TicketMapper.toResponse(ticket));
        }

        return dto;
    }
}
