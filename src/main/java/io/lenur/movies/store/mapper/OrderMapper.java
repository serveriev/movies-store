package io.lenur.movies.store.mapper;

import io.lenur.movies.store.domain.Order;
import io.lenur.movies.store.domain.Ticket;
import io.lenur.movies.store.dto.response.OrderResponseDto;

public class OrderMapper {
    public static OrderResponseDto toResponse(Order order) {
        OrderResponseDto dto = new OrderResponseDto();
        dto.setId(order.getId());
        dto.setStatus(order.getStatus());
        dto.setCreated(order.getCreated());
        dto.setUser(UserMapper.toResponse(order.getUser()));

        for (Ticket ticket : order.getTickets()) {
            dto.addTicket(TicketMapper.toResponse(ticket));
        }

        return dto;
    }
}
