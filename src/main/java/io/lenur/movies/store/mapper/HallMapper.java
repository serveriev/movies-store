package io.lenur.movies.store.mapper;

import io.lenur.movies.store.domain.Hall;
import io.lenur.movies.store.dto.response.HallResponseDto;

public class HallMapper {
    public static HallResponseDto toResponse(Hall hall) {
        HallResponseDto dto = new HallResponseDto();
        dto.setId(hall.getId());
        dto.setCapacity(hall.getCapacity());
        dto.setDescription(hall.getDescription());

        return dto;
    }

    public static Hall toDomainFromResponse(HallResponseDto dto) {
        Hall hall = new Hall();
        hall.setId(dto.getId());
        hall.setCapacity(dto.getCapacity());
        hall.setDescription(dto.getDescription());

        return hall;
    }
}
