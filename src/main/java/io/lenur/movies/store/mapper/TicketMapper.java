package io.lenur.movies.store.mapper;

import io.lenur.movies.store.domain.Ticket;
import io.lenur.movies.store.dto.response.TicketResponseDto;

public class TicketMapper {
    public static TicketResponseDto toResponse(Ticket ticket) {
        TicketResponseDto dto = new TicketResponseDto();
        dto.setId(ticket.getId());

        return dto;
    }
}
