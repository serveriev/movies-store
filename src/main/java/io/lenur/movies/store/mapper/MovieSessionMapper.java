package io.lenur.movies.store.mapper;

import io.lenur.movies.store.domain.MovieSession;
import io.lenur.movies.store.dto.response.MovieSessionResponseDto;

public class MovieSessionMapper {
    public static MovieSessionResponseDto toResponse(MovieSession session) {
        MovieSessionResponseDto dto = new MovieSessionResponseDto();
        dto.setId(session.getId());
        dto.setHall(HallMapper.toResponse(session.getHall()));
        dto.setTime(session.getTime());
        dto.setMovie(MovieMapper.toResponse(session.getMovie()));

        return dto;
    }
}
