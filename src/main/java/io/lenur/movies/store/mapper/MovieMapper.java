package io.lenur.movies.store.mapper;

import io.lenur.movies.store.domain.Movie;
import io.lenur.movies.store.dto.response.MovieResponseDto;

public class MovieMapper {
    public static MovieResponseDto toResponse(Movie movie) {
        MovieResponseDto dto = new MovieResponseDto();
        dto.setId(movie.getId());
        dto.setTitle(movie.getTitle());
        dto.setDescription(movie.getDescription());

        return dto;
    }

    public static Movie toDomainFromResponse(MovieResponseDto dto) {
        Movie movie = new Movie();
        movie.setId(dto.getId());
        movie.setTitle(dto.getTitle());
        movie.setDescription(dto.getDescription());

        return movie;
    }
}
