package io.lenur.movies.store.mapper;

import io.lenur.movies.store.domain.User;
import io.lenur.movies.store.dto.response.UserResponseDto;

public class UserMapper {
    public static UserResponseDto toResponse(User user) {
        UserResponseDto dto = new UserResponseDto();
        dto.setId(user.getId());
        dto.setEmail(user.getEmail());

        return dto;
    }
}
