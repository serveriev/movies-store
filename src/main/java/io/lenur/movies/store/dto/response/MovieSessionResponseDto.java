package io.lenur.movies.store.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

public class MovieSessionResponseDto {
    private Long id;

    private HallResponseDto hall;

    private MovieResponseDto movie;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime time;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public HallResponseDto getHall() {
        return hall;
    }

    public void setHall(HallResponseDto hall) {
        this.hall = hall;
    }

    public MovieResponseDto getMovie() {
        return movie;
    }

    public void setMovie(MovieResponseDto movie) {
        this.movie = movie;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }
}
