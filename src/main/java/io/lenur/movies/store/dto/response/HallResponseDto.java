package io.lenur.movies.store.dto.response;

public class HallResponseDto {
    private Long id;

    private int capacity;

    private String description;

    public int getCapacity() {
        return capacity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
