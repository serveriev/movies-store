package io.lenur.movies.store.dto.response;

public class TicketResponseDto {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
