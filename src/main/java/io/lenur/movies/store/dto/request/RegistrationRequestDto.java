package io.lenur.movies.store.dto.request;

import io.lenur.movies.store.domain.RoleName;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Set;

public class RegistrationRequestDto {
    @Email
    private String email;

    @NotEmpty
    private String password;

    @NotEmpty
    private Set<RoleName> roles = new HashSet<>();

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<RoleName> getRoles() {
        return roles;
    }

    public void setRoles(Set<RoleName> roles) {
        this.roles = roles;
    }
}
