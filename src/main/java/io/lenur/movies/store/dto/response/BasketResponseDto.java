package io.lenur.movies.store.dto.response;

import java.util.ArrayList;
import java.util.List;

public class BasketResponseDto {
    private Long id;

    private UserResponseDto user;

    private List<TicketResponseDto> tickets = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserResponseDto getUser() {
        return user;
    }

    public void setUser(UserResponseDto user) {
        this.user = user;
    }

    public List<TicketResponseDto> getTickets() {
        return tickets;
    }

    public void setTickets(List<TicketResponseDto> tickets) {
        this.tickets = tickets;
    }

    public void addTicket(TicketResponseDto ticket) {
        this.tickets.add(ticket);
    }
}
