package io.lenur.movies.store.controller;

import io.lenur.movies.store.dto.request.MovieRequestDto;
import io.lenur.movies.store.dto.response.MovieResponseDto;
import io.lenur.movies.store.service.api.ApiMovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/movies")
public class MovieController {
    @Autowired
    private ApiMovieService movieService;

    @GetMapping
    public List<MovieResponseDto> movies() {
        return movieService.getAll();
    }

    @GetMapping(value = "/{id}")
    public MovieResponseDto get(@PathVariable Long id) {
        return movieService.get(id);
    }

    @PostMapping
    public MovieResponseDto create(@Valid @RequestBody MovieRequestDto movieDto) {
        return movieService.create(movieDto);
    }
}

