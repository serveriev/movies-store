package io.lenur.movies.store.controller;

import io.lenur.movies.store.dto.response.UserResponseDto;
import io.lenur.movies.store.service.api.ApiUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/users")
public class UserController {
    @Autowired
    private ApiUserService userService;

    @GetMapping
    public List<UserResponseDto> users(@RequestParam(required = false) String email) {
        return userService.getAllByEmail(email);
    }

    @GetMapping(value = "/{id}")
    public UserResponseDto get(@PathVariable Long id) {
        return userService.get(id);
    }
}

