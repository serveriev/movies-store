package io.lenur.movies.store.controller;

import io.lenur.movies.store.dto.request.AuthenticationRequestDto;
import io.lenur.movies.store.dto.request.RegistrationRequestDto;
import io.lenur.movies.store.dto.response.AuthenticationResponseDto;
import io.lenur.movies.store.dto.response.UserResponseDto;
import io.lenur.movies.store.service.api.ApiAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class AuthenticationController {
    @Autowired
    private ApiAuthentication authentication;

    @PostMapping(value = "/registration")
    public UserResponseDto registration(@Valid @RequestBody RegistrationRequestDto dto) {
        return authentication.registration(dto);
    }

    @PostMapping(value = "/login")
    public AuthenticationResponseDto login(@Valid @RequestBody AuthenticationRequestDto dto) {
        return authentication.login(dto);
    }
}

