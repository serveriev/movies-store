package io.lenur.movies.store.controller;

import io.lenur.movies.store.dto.response.OrderResponseDto;
import io.lenur.movies.store.service.api.ApiOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/orders")
public class OrderController {
    @Autowired
    private ApiOrderService orderService;

    @GetMapping
    public List<OrderResponseDto> movies(
            @RequestParam(required = false, name = "user_id") Long userId) {
        return orderService.getByUser(userId);
    }

    //todo remove {user_id} after authorization
    @PostMapping(value = "/{userId}")
    public OrderResponseDto create(@PathVariable Long userId) {
        return orderService.create(userId);
    }
}

