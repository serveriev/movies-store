package io.lenur.movies.store.controller;

import io.lenur.movies.store.dto.request.HallRequestDto;
import io.lenur.movies.store.dto.response.HallResponseDto;
import io.lenur.movies.store.service.api.ApiHallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/halls")
public class HallController {
    @Autowired
    private ApiHallService hallService;

    @GetMapping
    public List<HallResponseDto> movies() {
        return hallService.getAll();
    }

    @GetMapping(value = "/{id}")
    public HallResponseDto get(@PathVariable Long id) {
        return hallService.get(id);
    }

    @PostMapping
    public HallResponseDto create(@Valid @RequestBody HallRequestDto hallDto) {
        return hallService.create(hallDto);
    }
}

