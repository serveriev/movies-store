package io.lenur.movies.store.controller;

import io.lenur.movies.store.dto.request.MovieSessionRequestDto;
import io.lenur.movies.store.dto.response.MovieSessionResponseDto;
import io.lenur.movies.store.service.api.ApiMovieSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping(value = "/movies-sessions")
public class MovieSessionController {
    @Autowired
    private ApiMovieSessionService sessionService;

    @GetMapping
    public List<MovieSessionResponseDto> movies(
            @RequestParam(required = false, name = "movie_id") Long movieId,
            @RequestParam(required = false)
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
        return sessionService.findByMovieAndDate(movieId, date);
    }

    @PostMapping
    public MovieSessionResponseDto create(@Valid @RequestBody MovieSessionRequestDto sessionDto) {
        return sessionService.create(sessionDto);
    }
}

