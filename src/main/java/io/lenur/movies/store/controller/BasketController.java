package io.lenur.movies.store.controller;

import io.lenur.movies.store.dto.request.BasketRequestDto;
import io.lenur.movies.store.dto.response.BasketResponseDto;
import io.lenur.movies.store.service.api.ApiBasketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/baskets")
public class BasketController {
    @Autowired
    private ApiBasketService basketService;

    //todo replace into session
    @GetMapping(value = "/user/{userId}")
    public BasketResponseDto get(@PathVariable Long userId) {
        return basketService.getByUser(userId);
    }

    //todo replace into session
    @PostMapping(value = "/user/{userId}")
    public ResponseEntity<String> addMovieSession(
            @PathVariable Long userId,
            @Valid @RequestBody BasketRequestDto basketDto
            ) {
        basketService.addMovieSession(basketDto, userId);
        return ResponseEntity.ok("OK");
    }
}

