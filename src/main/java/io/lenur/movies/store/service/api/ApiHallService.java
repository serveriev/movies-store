package io.lenur.movies.store.service.api;

import io.lenur.movies.store.dto.request.HallRequestDto;
import io.lenur.movies.store.dto.response.HallResponseDto;

import java.util.List;

public interface ApiHallService {
    HallResponseDto create(HallRequestDto hallDto);

    List<HallResponseDto> getAll();

    HallResponseDto get(Long id);
}
