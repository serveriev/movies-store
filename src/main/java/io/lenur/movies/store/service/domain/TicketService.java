package io.lenur.movies.store.service.domain;

import io.lenur.movies.store.domain.MovieSession;
import io.lenur.movies.store.domain.Ticket;
import io.lenur.movies.store.domain.User;

public interface TicketService {
    Ticket create(Ticket ticket);

    Ticket findBySessionAndUser(MovieSession movieSession, User user);

    void delete(Ticket ticket);
}
