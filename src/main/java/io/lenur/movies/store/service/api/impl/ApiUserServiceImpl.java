package io.lenur.movies.store.service.api.impl;

import io.lenur.movies.store.domain.User;
import io.lenur.movies.store.dto.response.UserResponseDto;
import io.lenur.movies.store.mapper.UserMapper;
import io.lenur.movies.store.service.api.ApiUserService;
import io.lenur.movies.store.service.domain.UserService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ApiUserServiceImpl implements ApiUserService {
    private final UserService userService;

    public ApiUserServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public List<UserResponseDto> getAllByEmail(String email) {
        List<User> users = userService.getAllByUser(email);

        return users
                .stream()
                .map(UserMapper::toResponse)
                .collect(Collectors.toList());
    }

    @Override
    public UserResponseDto get(Long id) {
        User user = userService.get(id);

        return UserMapper.toResponse(user);
    }
}
