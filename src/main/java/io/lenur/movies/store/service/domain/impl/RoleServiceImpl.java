package io.lenur.movies.store.service.domain.impl;

import io.lenur.movies.store.dao.RoleDao;
import io.lenur.movies.store.domain.Role;
import io.lenur.movies.store.domain.RoleName;
import io.lenur.movies.store.exception.DomainNotFoundException;
import io.lenur.movies.store.service.domain.RoleService;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {
    private final RoleDao roleDao;

    public RoleServiceImpl(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    @Override
    public Role getByName(RoleName name) {
        return roleDao
                .findByName(name)
                .orElseThrow(() -> new DomainNotFoundException("Role is not found!"));
    }
}
