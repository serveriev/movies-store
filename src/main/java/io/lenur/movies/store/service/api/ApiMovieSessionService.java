package io.lenur.movies.store.service.api;

import io.lenur.movies.store.dto.request.MovieSessionRequestDto;
import io.lenur.movies.store.dto.response.MovieSessionResponseDto;

import java.time.LocalDate;
import java.util.List;

public interface ApiMovieSessionService {
    MovieSessionResponseDto create(MovieSessionRequestDto sessionDto);

    List<MovieSessionResponseDto> findByMovieAndDate(Long movieId, LocalDate date);
}
