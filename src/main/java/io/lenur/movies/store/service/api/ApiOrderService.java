package io.lenur.movies.store.service.api;

import io.lenur.movies.store.dto.response.OrderResponseDto;

import java.util.List;

public interface ApiOrderService {
    OrderResponseDto create(Long userId);

    List<OrderResponseDto> getByUser(Long userId);
}
