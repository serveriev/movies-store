package io.lenur.movies.store.service.domain.impl;

import io.lenur.movies.store.dao.BasketDao;
import io.lenur.movies.store.domain.Basket;
import io.lenur.movies.store.domain.MovieSession;
import io.lenur.movies.store.domain.Ticket;
import io.lenur.movies.store.domain.User;
import io.lenur.movies.store.service.domain.BasketService;
import io.lenur.movies.store.service.domain.MovieService;
import io.lenur.movies.store.service.domain.MovieSessionService;
import io.lenur.movies.store.service.domain.TicketService;
import io.lenur.movies.store.service.domain.UserService;
import org.springframework.stereotype.Service;

@Service
public class BasketServiceImpl implements BasketService {
    private final BasketDao basketDao;
    private final TicketService ticketService;
    private final UserService userService;
    private final MovieSessionService movieSessionService;

    public BasketServiceImpl(
            BasketDao basketDao,
            TicketService ticketService,
            UserService userService,
            MovieSessionService movieSessionService) {
        this.basketDao = basketDao;
        this.ticketService = ticketService;
        this.userService = userService;
        this.movieSessionService = movieSessionService;
    }

    @Override
    public void addMovieSession(Long movieSessionId, Long userId) {
        User user = userService.get(userId);
        MovieSession session = movieSessionService.get(movieSessionId);

        Ticket ticket = new Ticket();
        ticket.setUser(user);
        ticket.setMovieSession(session);
        ticketService.create(ticket);

        Basket basket = basketDao.getByUser(user);
        basket.addTicket(ticket);
        basketDao.update(basket);
    }

    @Override
    public Basket getByUser(Long userId) {
        User user = userService.get(userId);
        return basketDao.getByUser(user);
    }

    @Override
    public Basket create(User user) {
        Basket basket = new Basket();
        basket.setUser(user);

        return basketDao.create(basket);
    }

    @Override
    public void update(Basket basket) {
        basketDao.update(basket);
    }
}
