package io.lenur.movies.store.service.api;

import io.lenur.movies.store.dto.response.UserResponseDto;

import java.util.List;

public interface ApiUserService {
    List<UserResponseDto> getAllByEmail(String email);

    UserResponseDto get(Long id);
}
