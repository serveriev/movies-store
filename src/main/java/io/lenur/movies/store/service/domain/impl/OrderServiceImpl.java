package io.lenur.movies.store.service.domain.impl;

import io.lenur.movies.store.dao.OrderDao;
import io.lenur.movies.store.domain.Basket;
import io.lenur.movies.store.domain.Order;
import io.lenur.movies.store.domain.User;
import io.lenur.movies.store.service.domain.BasketService;
import io.lenur.movies.store.service.domain.OrderService;
import io.lenur.movies.store.service.domain.UserService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    private final OrderDao orderDao;
    private final UserService userService;
    private final BasketService basketService;

    public OrderServiceImpl(
            OrderDao orderDao,
            UserService userService,
            BasketService basketService
    ) {
        this.orderDao = orderDao;
        this.userService = userService;
        this.basketService = basketService;
    }

    @Override
    public Order completeOrder(Long userId) {
        Basket basket = basketService.getByUser(userId);
        User user = userService.get(userId);

        Order order = new Order();
        order.setTickets(basket.getTickets());
        order.setCreated(LocalDateTime.now());
        order.setUser(user);
        orderDao.create(order);

        basket.setTickets(new ArrayList<>());
        basketService.update(basket);

        return order;
    }

    @Override
    public List<Order> getByUser(Long userId) {
        User user = null;
        if (userId != null) {
            user = userService.get(userId);
        }

        return orderDao.getByUser(user);
    }
}
