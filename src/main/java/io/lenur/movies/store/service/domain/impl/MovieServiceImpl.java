package io.lenur.movies.store.service.domain.impl;

import io.lenur.movies.store.dao.MovieDao;
import io.lenur.movies.store.domain.Movie;
import io.lenur.movies.store.exception.DomainNotFoundException;
import io.lenur.movies.store.service.domain.MovieService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovieServiceImpl implements MovieService {
    private final MovieDao movieDao;

    public MovieServiceImpl(MovieDao movieDao) {
        this.movieDao = movieDao;
    }

    @Override
    public Movie create(Movie movie) {
        return movieDao.create(movie);
    }

    @Override
    public List<Movie> getAll() {
        return movieDao.getAll();
    }

    @Override
    public Movie get(Long id) {
        return movieDao
                .get(id)
                .orElseThrow(() -> new DomainNotFoundException("Movie doesn't exist"));
    }
}
