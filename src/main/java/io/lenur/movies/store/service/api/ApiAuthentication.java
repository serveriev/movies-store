package io.lenur.movies.store.service.api;

import io.lenur.movies.store.dto.request.AuthenticationRequestDto;
import io.lenur.movies.store.dto.request.RegistrationRequestDto;
import io.lenur.movies.store.dto.response.AuthenticationResponseDto;
import io.lenur.movies.store.dto.response.UserResponseDto;

public interface ApiAuthentication {
    AuthenticationResponseDto login(AuthenticationRequestDto dto);

    UserResponseDto registration(RegistrationRequestDto dto);
}
