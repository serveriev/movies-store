package io.lenur.movies.store.service.domain;

import io.lenur.movies.store.domain.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    User create(User user);

    Optional<User> findByEmail(String email);

    List<User> getAllByUser(String email);

    User get(Long id);
}
