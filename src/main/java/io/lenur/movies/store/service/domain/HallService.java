package io.lenur.movies.store.service.domain;

import io.lenur.movies.store.domain.Hall;

import java.util.List;

public interface HallService {
    Hall create(Hall hall);

    List<Hall> getAll();

    Hall get(Long id);
}
