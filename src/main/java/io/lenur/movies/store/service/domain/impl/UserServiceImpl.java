package io.lenur.movies.store.service.domain.impl;

import io.lenur.movies.store.dao.UserDao;
import io.lenur.movies.store.domain.User;
import io.lenur.movies.store.exception.DomainNotFoundException;
import io.lenur.movies.store.service.domain.UserService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private final UserDao userDao;

    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public User create(User user) {
        return userDao.create(user);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userDao.findByEmail(email);
    }

    @Override
    public User get(Long id) {
        return userDao
                .get(id)
                .orElseThrow(() -> new DomainNotFoundException("User doesn't exist"));
    }

    @Override
    public List<User> getAllByUser(String email) {
        return userDao.getAllByUser(email);
    }
}
