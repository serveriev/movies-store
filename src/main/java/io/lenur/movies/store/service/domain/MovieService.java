package io.lenur.movies.store.service.domain;

import io.lenur.movies.store.domain.Movie;

import java.util.List;

public interface MovieService {
    Movie create(Movie movie);

    List<Movie> getAll();

    Movie get(Long id);
}
