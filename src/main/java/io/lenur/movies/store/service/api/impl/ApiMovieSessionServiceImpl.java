package io.lenur.movies.store.service.api.impl;

import io.lenur.movies.store.domain.Hall;
import io.lenur.movies.store.domain.Movie;
import io.lenur.movies.store.domain.MovieSession;
import io.lenur.movies.store.dto.request.MovieSessionRequestDto;
import io.lenur.movies.store.dto.response.MovieSessionResponseDto;
import io.lenur.movies.store.mapper.MovieSessionMapper;
import io.lenur.movies.store.service.api.ApiMovieSessionService;
import io.lenur.movies.store.service.domain.HallService;
import io.lenur.movies.store.service.domain.MovieService;
import io.lenur.movies.store.service.domain.MovieSessionService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ApiMovieSessionServiceImpl implements ApiMovieSessionService {
    private final MovieSessionService sessionService;
    private final HallService hallService;
    private final MovieService movieService;

    public ApiMovieSessionServiceImpl(
            MovieSessionService sessionService,
            HallService hallService,
            MovieService movieService) {
        this.sessionService = sessionService;
        this.hallService = hallService;
        this.movieService = movieService;
    }

    @Override
    public MovieSessionResponseDto create(MovieSessionRequestDto sessionDto) {
        Hall hall = hallService.get(sessionDto.getHallId());
        Movie movie = movieService.get(sessionDto.getMovieId());

        MovieSession session = new MovieSession();
        session.setHall(hall);
        session.setTime(sessionDto.getTime());
        session.setMovie(movie);

        sessionService.create(session);

        return MovieSessionMapper.toResponse(session);
    }

    @Override
    public List<MovieSessionResponseDto> findByMovieAndDate(Long movieId, LocalDate date) {
        return sessionService
                .findByMovieAndDate(movieId, date)
                .stream()
                .map(MovieSessionMapper::toResponse)
                .collect(Collectors.toList());
    }
}
