package io.lenur.movies.store.service.api;

import io.lenur.movies.store.dto.request.MovieRequestDto;
import io.lenur.movies.store.dto.response.MovieResponseDto;

import java.util.List;

public interface ApiMovieService {
    MovieResponseDto create(MovieRequestDto movieDto);

    List<MovieResponseDto> getAll();

    MovieResponseDto get(Long id);
}
