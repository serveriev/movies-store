package io.lenur.movies.store.service.api.impl;

import io.lenur.movies.store.domain.User;
import io.lenur.movies.store.dto.request.AuthenticationRequestDto;
import io.lenur.movies.store.dto.request.RegistrationRequestDto;
import io.lenur.movies.store.dto.response.AuthenticationResponseDto;
import io.lenur.movies.store.dto.response.UserResponseDto;
import io.lenur.movies.store.mapper.UserMapper;
import io.lenur.movies.store.security.AppAuthentication;
import io.lenur.movies.store.service.api.ApiAuthentication;
import org.springframework.stereotype.Service;

@Service
public class ApiAuthenticationImpl implements ApiAuthentication {
    private final AppAuthentication appAuthentication;

    public ApiAuthenticationImpl(AppAuthentication appAuthentication) {
        this.appAuthentication = appAuthentication;
    }

    @Override
    public AuthenticationResponseDto login(AuthenticationRequestDto dto) {
        String email = dto.getEmail();
        String password = dto.getPassword();
        String token = appAuthentication.login(email, password);

        return new AuthenticationResponseDto(token);
    }

    @Override
    public UserResponseDto registration(RegistrationRequestDto dto) {
        User user = appAuthentication.registration(dto);
        return UserMapper.toResponse(user);
    }
}
