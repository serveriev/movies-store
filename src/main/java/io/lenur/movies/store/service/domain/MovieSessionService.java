package io.lenur.movies.store.service.domain;

import io.lenur.movies.store.domain.MovieSession;

import java.time.LocalDate;
import java.util.List;

public interface MovieSessionService {
    MovieSession create(MovieSession session);

    List<MovieSession> findByMovieAndDate(Long movieId, LocalDate date);

    MovieSession get(Long id);
}
