package io.lenur.movies.store.service.domain.impl;

import io.lenur.movies.store.dao.MovieSessionDao;
import io.lenur.movies.store.domain.Movie;
import io.lenur.movies.store.domain.MovieSession;
import io.lenur.movies.store.exception.DomainNotFoundException;
import io.lenur.movies.store.service.domain.MovieService;
import io.lenur.movies.store.service.domain.MovieSessionService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class MovieSessionServiceImpl implements MovieSessionService {
    private final MovieSessionDao sessionDao;
    private final MovieService movieService;

    public MovieSessionServiceImpl(MovieSessionDao sessionDao, MovieService movieService) {
        this.sessionDao = sessionDao;
        this.movieService = movieService;
    }

    @Override
    public MovieSession create(MovieSession session) {
        return sessionDao.create(session);
    }

    @Override
    public List<MovieSession> findByMovieAndDate(Long movieId, LocalDate date) {
        Movie movie = null;
        if (movieId != null) {
            movie = movieService.get(movieId);
        }

        return sessionDao.findByMovieAndDate(movie, date);
    }

    @Override
    public MovieSession get(Long id) {
        return sessionDao
                .get(id)
                .orElseThrow(() -> new DomainNotFoundException("Movie session doesn't exist"));
    }
}
