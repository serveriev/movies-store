package io.lenur.movies.store.service.api.impl;

import io.lenur.movies.store.domain.Movie;
import io.lenur.movies.store.dto.request.MovieRequestDto;
import io.lenur.movies.store.dto.response.MovieResponseDto;
import io.lenur.movies.store.mapper.MovieMapper;
import io.lenur.movies.store.service.api.ApiMovieService;
import io.lenur.movies.store.service.domain.MovieService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ApiMovieServiceImpl implements ApiMovieService {
    private final MovieService movieService;

    public ApiMovieServiceImpl(MovieService movieService) {
        this.movieService = movieService;
    }

    @Override
    public MovieResponseDto create(MovieRequestDto movieDto) {
        Movie movie = new Movie();
        movie.setTitle(movieDto.getTitle());
        movie.setDescription(movieDto.getDescription());

        movieService.create(movie);

        return MovieMapper.toResponse(movie);
    }

    @Override
    public List<MovieResponseDto> getAll() {
        return movieService
                .getAll()
                .stream()
                .map(MovieMapper::toResponse)
                .collect(Collectors.toList());
    }

    @Override
    public MovieResponseDto get(Long id) {
        Movie movie = movieService.get(id);

        return MovieMapper.toResponse(movie);
    }
}
