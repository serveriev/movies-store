package io.lenur.movies.store.service.domain.impl;

import io.lenur.movies.store.dao.TicketDao;
import io.lenur.movies.store.domain.MovieSession;
import io.lenur.movies.store.domain.Ticket;
import io.lenur.movies.store.domain.User;
import io.lenur.movies.store.service.domain.TicketService;
import org.springframework.stereotype.Service;

@Service
public class TicketServiceImpl implements TicketService {
    private final TicketDao ticketDao;

    public TicketServiceImpl(TicketDao ticketDao) {
        this.ticketDao = ticketDao;
    }

    @Override
    public Ticket create(Ticket ticket) {
        return ticketDao.create(ticket);
    }

    @Override
    public Ticket findBySessionAndUser(MovieSession movieSession, User user) {
        return ticketDao.findBySessionAndUser(movieSession, user);
    }

    @Override
    public void delete(Ticket ticket) {
        ticketDao.delete(ticket);
    }
}
