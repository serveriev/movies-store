package io.lenur.movies.store.service.api.impl;

import io.lenur.movies.store.domain.Hall;
import io.lenur.movies.store.dto.request.HallRequestDto;
import io.lenur.movies.store.dto.response.HallResponseDto;
import io.lenur.movies.store.mapper.HallMapper;
import io.lenur.movies.store.service.api.ApiHallService;
import io.lenur.movies.store.service.domain.HallService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ApiHallServiceImpl implements ApiHallService {
    private final HallService hallService;

    public ApiHallServiceImpl(HallService hallService) {
        this.hallService = hallService;
    }

    @Override
    public HallResponseDto create(HallRequestDto hallDto) {
        Hall hall = new Hall();
        hall.setCapacity(hallDto.getCapacity());
        hall.setDescription(hallDto.getDescription());

        hallService.create(hall);

        return HallMapper.toResponse(hall);
    }

    @Override
    public List<HallResponseDto> getAll() {
        return hallService
                .getAll()
                .stream()
                .map(HallMapper::toResponse)
                .collect(Collectors.toList());
    }

    @Override
    public HallResponseDto get(Long id) {
        Hall hall = hallService.get(id);

        return HallMapper.toResponse(hall);
    }
}
