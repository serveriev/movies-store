package io.lenur.movies.store.service.domain;

import io.lenur.movies.store.domain.Basket;
import io.lenur.movies.store.domain.MovieSession;
import io.lenur.movies.store.domain.User;

public interface BasketService {
    void addMovieSession(Long movieSessionId, Long userId);

    Basket getByUser(Long userId);

    Basket create(User user);

    void update(Basket basket);
}
