package io.lenur.movies.store.service.api;

import io.lenur.movies.store.dto.request.BasketRequestDto;
import io.lenur.movies.store.dto.response.BasketResponseDto;

public interface ApiBasketService {
    BasketResponseDto getByUser(Long userId);

    void addMovieSession(BasketRequestDto basketDto, Long userId);
}
