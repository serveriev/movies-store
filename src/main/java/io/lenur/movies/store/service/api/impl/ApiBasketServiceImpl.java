package io.lenur.movies.store.service.api.impl;

import io.lenur.movies.store.domain.Basket;
import io.lenur.movies.store.dto.request.BasketRequestDto;
import io.lenur.movies.store.dto.response.BasketResponseDto;
import io.lenur.movies.store.mapper.BasketMapper;
import io.lenur.movies.store.service.api.ApiBasketService;
import io.lenur.movies.store.service.domain.BasketService;
import org.springframework.stereotype.Service;

@Service
public class ApiBasketServiceImpl implements ApiBasketService {
    private final BasketService basketService;

    public ApiBasketServiceImpl(BasketService basketService) {
        this.basketService = basketService;
    }

    @Override
    public BasketResponseDto getByUser(Long userId) {
        Basket basket = basketService.getByUser(userId);

        return BasketMapper.toResponse(basket);
    }

    @Override
    public void addMovieSession(BasketRequestDto basketDto, Long userId) {
        Long movieSessionId = basketDto.getMovieSessionId();
        basketService.addMovieSession(movieSessionId, userId);
    }
}
