package io.lenur.movies.store.service.api.impl;

import io.lenur.movies.store.domain.Order;
import io.lenur.movies.store.dto.response.OrderResponseDto;
import io.lenur.movies.store.mapper.OrderMapper;
import io.lenur.movies.store.service.api.ApiOrderService;
import io.lenur.movies.store.service.domain.OrderService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ApiOrderServiceImpl implements ApiOrderService {
    private final OrderService orderService;

    public ApiOrderServiceImpl(OrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    public OrderResponseDto create(Long userId) {
        Order order = orderService.completeOrder(userId);
        return OrderMapper.toResponse(order);
    }

    @Override
    public List<OrderResponseDto> getByUser(Long userId) {
        return orderService
                .getByUser(userId)
                .stream()
                .map(OrderMapper::toResponse)
                .collect(Collectors.toList());
    }
}
