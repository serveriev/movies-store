package io.lenur.movies.store.service.domain;

import io.lenur.movies.store.domain.Order;

import java.util.List;

public interface OrderService {
      Order completeOrder(Long userId);
            
      List<Order> getByUser(Long userId);
  }