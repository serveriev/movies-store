package io.lenur.movies.store.service.domain;

import io.lenur.movies.store.domain.Role;
import io.lenur.movies.store.domain.RoleName;

public interface RoleService {
    Role getByName(RoleName name);
}
