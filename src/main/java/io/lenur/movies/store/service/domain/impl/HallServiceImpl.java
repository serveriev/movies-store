package io.lenur.movies.store.service.domain.impl;

import io.lenur.movies.store.dao.HallDao;
import io.lenur.movies.store.domain.Hall;
import io.lenur.movies.store.exception.DomainNotFoundException;
import io.lenur.movies.store.service.domain.HallService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HallServiceImpl implements HallService {
    private final HallDao hallDao;

    public HallServiceImpl(HallDao hallDao) {
        this.hallDao = hallDao;
    }

    @Override
    public Hall create(Hall hall) {
        return hallDao.create(hall);
    }

    @Override
    public List<Hall> getAll() {
        return hallDao.getAll();
    }

    @Override
    public Hall get(Long id) {
        return hallDao
                .get(id)
                .orElseThrow(() -> new DomainNotFoundException("Hall doesn't exist"));
    }
}
