package io.lenur.movies.store.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class SecurityApplicationInitializer extends AbstractSecurityWebApplicationInitializer {
}
