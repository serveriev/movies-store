package io.lenur.movies.store.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {
    "io.lenur.movies.store.service",
    "io.lenur.movies.store.dao",
    "io.lenur.movies.store.security",
    "io.lenur.movies.store.exception"
})
public class ApplicationConfig {
}
