package io.lenur.movies.store.dao.impl;

import io.lenur.movies.store.dao.MovieSessionDao;
import io.lenur.movies.store.domain.Movie;
import io.lenur.movies.store.domain.MovieSession;
import io.lenur.movies.store.exception.DaoException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Repository
public class MovieSessionDaoImpl implements MovieSessionDao {
    @Autowired
    protected SessionFactory sessionFactory;

    @Override
    public MovieSession create(MovieSession movieSession) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(movieSession);
            transaction.commit();
            return movieSession;
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            String msg = "Can't create a movie entity!";
            throw new DaoException(msg, e);
        }
    }

    @Override
    public List<MovieSession> findByMovieAndDate(Movie movie, LocalDate date) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery(
             "SELECT ms " +
                "FROM MovieSession ms " +
                "JOIN FETCH ms.movie " +
                "JOIN FETCH ms.hall " +
                "WHERE (:movie IS NULL OR ms.movie=:movie) " +
                     "AND (:date IS NULL OR ms.time BETWEEN :startDate AND :endDate)"
                    , MovieSession.class);

            query.setParameter("date", date);
            if (date == null) {
                date = LocalDate.now();
            }

            query.setParameter("startDate", date.atStartOfDay());
            query.setParameter("endDate", date.atTime(LocalTime.MAX));
            query.setParameter("movie", movie);

            return query.getResultList();
        } catch (HibernateException e) {
            String msg = "There is something wrong in getting all movies";
            throw new DaoException(msg, e);
        }
    }

    @Override
    public Optional<MovieSession> get(Long id) {
        try (Session session = sessionFactory.openSession()) {
            return Optional.ofNullable(session.get(MovieSession.class, id));
        } catch (HibernateException e) {
            String msg = "Can't fetch a movie session!";
            throw new DaoException(msg, e);
        }
    }
}
