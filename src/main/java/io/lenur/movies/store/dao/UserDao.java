package io.lenur.movies.store.dao;

import io.lenur.movies.store.domain.User;

import java.util.List;
import java.util.Optional;

public interface UserDao {
    User create(User user);

    Optional<User> findByEmail(String email);

    List<User> getAllByUser(String email);

    Optional<User> get(Long id);
}
