package io.lenur.movies.store.dao;

import io.lenur.movies.store.domain.Hall;

import java.util.List;
import java.util.Optional;

public interface HallDao {
    Hall create(Hall hall);

    List<Hall> getAll();

    Optional<Hall> get(Long id);
}
