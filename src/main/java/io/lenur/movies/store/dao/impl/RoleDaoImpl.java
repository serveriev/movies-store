package io.lenur.movies.store.dao.impl;

import io.lenur.movies.store.dao.RoleDao;
import io.lenur.movies.store.domain.Role;
import io.lenur.movies.store.domain.RoleName;
import io.lenur.movies.store.exception.DaoException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.Optional;

@Repository
public class RoleDaoImpl implements RoleDao {
    @Autowired
    protected SessionFactory sessionFactory;

    @Override
    public Optional<Role> findByName(RoleName name) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery(
                    "SELECT r " +
                        "FROM Role r " +
                        "WHERE r.name=:name"
                    , Role.class);
            query.setParameter("name", name);
            Role role = (Role) query.getSingleResult();
            return Optional.ofNullable(role);
        } catch (HibernateException e) {
            String msg = "There is something wrong in getting role";
            throw new DaoException(msg, e);
        }
    }
}
