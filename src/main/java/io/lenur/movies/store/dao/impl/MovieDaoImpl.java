package io.lenur.movies.store.dao.impl;

import io.lenur.movies.store.dao.MovieDao;
import io.lenur.movies.store.domain.Hall;
import io.lenur.movies.store.domain.Movie;
import io.lenur.movies.store.exception.DaoException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import java.util.Optional;

@Repository
public class MovieDaoImpl implements MovieDao {
    @Autowired
    protected SessionFactory sessionFactory;

    @Override
    public Movie create(Movie movie) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(movie);
            transaction.commit();
            return movie;
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            String msg = "Can't create a movie entity!";
            throw new DaoException(msg, e);
        }
    }

    @Override
    public List<Movie> getAll() {
        try (Session session = sessionFactory.openSession()) {
            CriteriaQuery<Movie> criteriaQuery = session
                    .getCriteriaBuilder()
                    .createQuery(Movie.class);
            criteriaQuery.from(Movie.class);

            return session.createQuery(criteriaQuery).getResultList();
        } catch (HibernateException e) {
            String msg = "There is something wrong in getting all movies";
            throw new DaoException(msg, e);
        }
    }

    @Override
    public Optional<Movie> get(Long id) {
        try (Session session = sessionFactory.openSession()) {
            return Optional.ofNullable(session.get(Movie.class, id));
        } catch (HibernateException e) {
            String msg = "Can't fetch a movie!";
            throw new DaoException(msg, e);
        }
    }
}
