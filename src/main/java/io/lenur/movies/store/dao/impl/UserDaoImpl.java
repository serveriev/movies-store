package io.lenur.movies.store.dao.impl;

import io.lenur.movies.store.dao.UserDao;
import io.lenur.movies.store.domain.User;
import io.lenur.movies.store.exception.DaoException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

@Repository
public class UserDaoImpl implements UserDao {
    @Autowired
    protected SessionFactory sessionFactory;

    @Override
    public User create(User user) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(user);
            transaction.commit();
            return user;
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            String msg = "Can't create an user entity!";
            throw new DaoException(msg, e);
        }
    }

    @Override
    public Optional<User> findByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery(
                    "SELECT u " +
                        "FROM User u " +
                        "LEFT JOIN FETCH u.roles " +
                        "WHERE u.email=:email"
                    , User.class);
            query.setParameter("email", email);
            User user = (User) query.getSingleResult();

            return Optional.ofNullable(user);
        } catch (HibernateException e) {
            String msg = "There is something wrong in getting user";
            throw new DaoException(msg, e);
        }
    }

    @Override
    public List<User> getAllByUser(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery(
                    "SELECT u " +
                        "FROM User u " +
                        "WHERE (:email IS NULL OR u.email=:email)"
                    , User.class);
            query.setParameter("email", email);

            return query.getResultList();
        } catch (HibernateException e) {
            String msg = "There is something wrong in getting all users";
            throw new DaoException(msg, e);
        }
    }

    @Override
    public Optional<User> get(Long id) {
        try (Session session = sessionFactory.openSession()) {
            return Optional.ofNullable(session.get(User.class, id));
        } catch (HibernateException e) {
            String msg = "Can't fetch an user!";
            throw new DaoException(msg, e);
        }
    }
}
