package io.lenur.movies.store.dao.impl;

import io.lenur.movies.store.dao.HallDao;
import io.lenur.movies.store.domain.Hall;
import io.lenur.movies.store.exception.DaoException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import java.util.Optional;

@Repository
public class HallDaoImpl implements HallDao {
    @Autowired
    protected SessionFactory sessionFactory;

    @Override
    public Hall create(Hall hall) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(hall);
            transaction.commit();
            return hall;
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            String msg = "Can't create a movie entity!";
            throw new DaoException(msg, e);
        }
    }

    @Override
    public List<Hall> getAll() {
        try (Session session = sessionFactory.openSession()) {
            CriteriaQuery<Hall> criteriaQuery = session
                    .getCriteriaBuilder()
                    .createQuery(Hall.class);
            criteriaQuery.from(Hall.class);
            return session.createQuery(criteriaQuery).getResultList();
        } catch (HibernateException e) {
            String msg = "There is something wrong in getting all movies";
            throw new DaoException(msg, e);
        }
    }

    @Override
    public Optional<Hall> get(Long id) {
        try (Session session = sessionFactory.openSession()) {
            return Optional.ofNullable(session.get(Hall.class, id));
        } catch (HibernateException e) {
            String msg = "Can't fetch a hall!";
            throw new DaoException(msg, e);
        }
    }
}
