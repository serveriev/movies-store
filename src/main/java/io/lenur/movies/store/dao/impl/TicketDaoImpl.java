package io.lenur.movies.store.dao.impl;

import io.lenur.movies.store.dao.TicketDao;
import io.lenur.movies.store.domain.MovieSession;
import io.lenur.movies.store.domain.Ticket;
import io.lenur.movies.store.domain.User;
import io.lenur.movies.store.exception.DaoException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;

@Repository
public class TicketDaoImpl implements TicketDao {
    @Autowired
    protected SessionFactory sessionFactory;

    @Override
    public Ticket create(Ticket ticket) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(ticket);
            transaction.commit();
            return ticket;
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            String msg = "Can't create a ticket entity!";
            throw new DaoException(msg, e);
        }
    }

    @Override
    public Ticket findBySessionAndUser(MovieSession movieSession, User user) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery(
                    "SELECT t " +
                        "FROM Ticket t " +
                        "JOIN FETCH t.movieSession " +
                        "JOIN FETCH t.user " +
                        "WHERE t.user=:user AND t.movieSession=:movieSession"
                    , Ticket.class);

            query.setParameter("user", user);
            query.setParameter("movieSession", movieSession);

            return (Ticket) query.getSingleResult();
        } catch (HibernateException e) {
            String msg = "There is something wrong in getting ticket";
            throw new DaoException(msg, e);
        }
    }

    @Override
    public void delete(Ticket ticket) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.delete(ticket);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            String msg = "Can't delete a ticket entity!";
            throw new DaoException(msg, e);
        }
    }
}
