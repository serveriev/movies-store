package io.lenur.movies.store.dao.impl;

import io.lenur.movies.store.dao.BasketDao;
import io.lenur.movies.store.domain.Basket;
import io.lenur.movies.store.domain.User;
import io.lenur.movies.store.exception.DaoException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;

@Repository
public class BasketDaoImpl implements BasketDao {
    @Autowired
    protected SessionFactory sessionFactory;

    @Override
    public Basket create(Basket basket) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(basket);
            transaction.commit();
            return basket;
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            String msg = "Can't create a basket entity!";
            throw new DaoException(msg, e);
        }
    }

    @Override
    public Basket getByUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery(
                    "SELECT b " +
                        "FROM Basket b " +
                        "JOIN FETCH b.user " +
                        "LEFT JOIN FETCH b.tickets " +
                        "WHERE b.user=:user"
                    , Basket.class);
            query.setParameter("user", user);
            return (Basket) query.getSingleResult();
        } catch (HibernateException e) {
            String msg = "There is something wrong in getting user's basket";
            throw new DaoException(msg, e);
        }
    }

    @Override
    public void update(Basket basket) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.update(basket);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            String msg = "Can't create a basket entity!";
            throw new DaoException(msg, e);
        }
    }
}
