package io.lenur.movies.store.dao;

import io.lenur.movies.store.domain.Movie;

import java.util.List;
import java.util.Optional;

public interface MovieDao {
    Movie create(Movie movie);

    List<Movie> getAll();

    Optional<Movie> get(Long id);
}
