package io.lenur.movies.store.dao.impl;

import io.lenur.movies.store.dao.OrderDao;
import io.lenur.movies.store.domain.Order;
import io.lenur.movies.store.domain.User;
import io.lenur.movies.store.exception.DaoException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
public class OrderDaoImpl implements OrderDao {
    @Autowired
    protected SessionFactory sessionFactory;

    @Override
    public Order create(Order order) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(order);
            transaction.commit();
            return order;
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            String msg = "Can't create a order entity!";
            throw new DaoException(msg, e);
        }
    }

    @Override
    public List<Order> getByUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery(
                     "SELECT o " +
                        "FROM Order o " +
                        "JOIN FETCH o.user " +
                        "LEFT JOIN FETCH o.tickets " +
                        "WHERE (:user IS NULL OR o.user=:user)"
                    , Order.class);
            query.setParameter("user", user);
            return query.getResultList();
        } catch (HibernateException e) {
            String msg = "There is something wrong in getting orders";
            throw new DaoException(msg, e);
        }
    }

    @Override
    public void update(Order order) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.update(order);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            String msg = "Can't create a order entity!";
            throw new DaoException(msg, e);
        }
    }
}
