package io.lenur.movies.store.dao;

import io.lenur.movies.store.domain.Order;
import io.lenur.movies.store.domain.User;

import java.util.List;

public interface OrderDao {
    Order create(Order order);

    List<Order> getByUser(User user);

    void update(Order order);
}
