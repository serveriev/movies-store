package io.lenur.movies.store.dao;

import io.lenur.movies.store.domain.Role;
import io.lenur.movies.store.domain.RoleName;

import java.util.Optional;

public interface RoleDao {
    Optional<Role> findByName(RoleName name);

}
