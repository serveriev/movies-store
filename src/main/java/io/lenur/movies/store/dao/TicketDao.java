package io.lenur.movies.store.dao;

import io.lenur.movies.store.domain.MovieSession;
import io.lenur.movies.store.domain.Ticket;
import io.lenur.movies.store.domain.User;

public interface TicketDao {
    Ticket create(Ticket ticket);

    Ticket findBySessionAndUser(MovieSession movieSession, User user);

    void delete(Ticket ticket);
}