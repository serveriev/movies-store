package io.lenur.movies.store.dao;

import io.lenur.movies.store.domain.Basket;
import io.lenur.movies.store.domain.User;

public interface BasketDao {
    Basket create(Basket basket);

    Basket getByUser(User user);

    void update(Basket basket);
}
