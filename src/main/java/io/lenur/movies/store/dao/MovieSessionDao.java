package io.lenur.movies.store.dao;

import io.lenur.movies.store.domain.Movie;
import io.lenur.movies.store.domain.MovieSession;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface MovieSessionDao {
    MovieSession create(MovieSession movieSession);

    List<MovieSession> findByMovieAndDate(Movie movie, LocalDate date);

    Optional<MovieSession> get(Long id);
}
