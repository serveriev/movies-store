package io.lenur.movies.store.exception;

public class DomainNotFoundException extends RuntimeException {
    public DomainNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public DomainNotFoundException(String message) {
        super(message);
    }
}
