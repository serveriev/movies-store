package io.lenur.movies.store.domain;

public enum OrderStatus {
    NEW("N"),
    PROCESSING("P"),
    REJECT("R"),
    COMPLETED("C");

    private final String code;

    OrderStatus(String code) {
        this.code = code;
    }
}

