package io.lenur.movies.store.domain;

public enum RoleName {
    USER,
    ADMIN
}
